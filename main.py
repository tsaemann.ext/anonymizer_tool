import argparse
import os

from Facebox_detector import FaceboxDetector
from MTCNN_detector import MTCNNDetector
from RetinaFace_detector import RetinaFaceDetector
from Pedestrian_detector import PedestrianDetector
from utils import face_boxes_2_org_img, visualize_boxes, blur_faces, imread_cv2


def main():

    parser = argparse.ArgumentParser()

    # Define the command-line arguments
    parser.add_argument("--input-path", type=str, help="Path to the folder with images that are to be anonymized.")
    parser.add_argument("--output-path", type=str, help="Path where the blurred images are to be saved.")
    parser.add_argument("--face-detectors", type=str, default="MTCNN,FaceBoxes,Retina", required=False,
                        help="Face detector/detectors that should be used. Possible options: MTCNN,FaceBoxes,Retina")
    parser.add_argument("--debug-visualize-boxes", type=str, default=False, required=False,
                        help="Visualize person and face bounding boxes.")

    # Parse the command-line arguments
    args = parser.parse_args()
    # Split the comma-separated string into a list
    args.face_detectors = args.face_detectors.split(',')

    # Image file extensions that are to be considered
    image_extensions = ['jpg', 'jpeg', 'png', 'bmp']

    # Get a list of all files in the folder
    all_files = os.listdir(args.input_path)

    # Filter the files based on the specified extensions
    image_paths = [os.path.join(args.input_path, file) for file in all_files if
                   file.lower().split('.')[-1] in image_extensions]

    # Pedestrian detector
    pedestrian_detector = PedestrianDetector()
    # MTCNN face detector
    mtcnn_face_detector = MTCNNDetector()
    detected_faces_mtcnn = None
    # Facebox face detector
    facebox_face_detector = FaceboxDetector()
    detected_faces_facebox = None
    # Retina face detector
    retinaface_face_detector = RetinaFaceDetector()
    detected_faces_retina = None

    # Iterate over the image files
    for index, image_file in enumerate(image_paths):
        print('Process image number: ', index)

        # Read in image with cv2 and convert it to RGB
        image = imread_cv2(image_file)

        detected_ped_boxes, detected_labels, cropped_ped_images = pedestrian_detector.detect_pedestrians(image)

        # Visualization and saving of pedestrian bounding boxes (for debugging purposes)
        if args.debug_visualize_boxes:
            # pedestrian_detector.save_cropped_images(cropped_ped_images, args.output_path)
            pedestrian_detector.visualize_pedestrian_detection(image, image_file, detected_ped_boxes, detected_labels,
                                                               args.output_path)
        if 'MTCNN' in args.face_detectors:
            detected_faces_mtcnn = mtcnn_face_detector.get_bbox_detection(cropped_ped_images)
            # Visualization of face bounding boxes (for debugging purposes)
            if args.debug_visualize_boxes:
                mtcnn_face_detector.visualize_face_detection(image, image_file, detected_faces_mtcnn, detected_ped_boxes,
                                                             args.output_path)

        if 'FaceBoxes' in args.face_detectors:
            results_list, pre_image = facebox_face_detector.get_bbox_detection(cropped_ped_images)
            detected_faces_facebox = facebox_face_detector.post_processing(results_list, pre_image)
            # Visualization of face bounding boxes (for debugging purposes)
            if args.debug_visualize_boxes:
                facebox_face_detector.visualize_face_detection(image, image_file, detected_faces_facebox,
                                                               detected_ped_boxes, args.output_path)

        if 'Retina' in args.face_detectors:
            detected_faces_retina = retinaface_face_detector.get_bbox_detection(cropped_ped_images)
            # Visualization of face bounding boxes (for debugging purposes)
            if args.debug_visualize_boxes:
                retinaface_face_detector.visualize_face_detection(image, image_file, detected_faces_retina,
                                                                  detected_ped_boxes, args.output_path)

        # Transfer box coordinates from both detectors bach into the original image
        final_boxes = face_boxes_2_org_img(detected_ped_boxes, detected_faces_mtcnn, detected_faces_facebox,
                                           detected_faces_retina)

        # Visualize transferred bounding boxes (for debugging purposes)
        if args.debug_visualize_boxes:
            visualize_boxes(image, image_file, final_boxes, args.output_path)

        # Blur the area inside the bounding box and save the final/blurred image
        blur_faces(image, image_file, final_boxes, args.output_path, 5)


if __name__ == "__main__":
    main()
