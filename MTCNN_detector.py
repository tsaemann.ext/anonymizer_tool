import cv2
import numpy as np
import torch

from facenet_pytorch import MTCNN
from utils import resize_images, ensure_directory_exists


class MTCNNDetector:
    def __init__(self):
        self.detector = MTCNN(thresholds=[0.15, 0.15, 0.15], keep_all=True, device='cuda').eval()
        self.detector = self.detector.cuda()

    @staticmethod
    def _pre_processing_mtcnn(image):
        image = resize_images(image, 128)  # 128 worked pretty well for Kitti images
        image = [(np.float32(element[0]), *element[1:]) for element in image]
        image = [(torch.from_numpy(element[0]).unsqueeze(0).to(torch.device("cuda")), *element[1:]) for element in
                 image]
        image = [(element[0].to(torch.device("cuda")), *element[1:]) for element in image]
        return image

    def get_bbox_detection(self, image):
        results_list = []
        pre_image = self._pre_processing_mtcnn(image)
        for img in pre_image:
            _, _, box, _, probs = self.detector(img[0])
            result_tuple = (box, probs, *img[1:])
            results_list.append(result_tuple)
        return results_list

    @staticmethod
    def visualize_face_detection(image_rgb, image_path, detected_faces, detected_ped_boxes, output_path):
        image_rgb_copy = np.copy(image_rgb)  # copy to avoid changing image outside this function (ndarray->mutable)
        # Plot bounding boxes
        for index, (faces, ped) in enumerate(zip(detected_faces, detected_ped_boxes)):
            if faces[1] is not None:
                face_x1, face_y1, face_x2, face_y2 = faces[0][0][0]
                ped_x1, ped_y1, ped_x2, ped_y2 = ped
                new_x1 = ped_x1 + face_x1 * faces[5]
                new_x2 = ped_x1 + face_x2 * faces[5]
                new_y1 = ped_y1 + face_y1 * faces[4]
                new_y2 = ped_y1 + face_y2 * faces[4]

                cv2.rectangle(image_rgb_copy, (round(new_x1), round(new_y1)), (round(new_x2), round(new_y2)),
                              (255, 0, 0), 2)
            else:
                continue

        image_rgb_copy = cv2.cvtColor(image_rgb_copy, cv2.COLOR_RGB2BGR)
        ensure_directory_exists(output_path + '/MTCNN/')
        save_path = output_path + '/MTCNN/' + image_path.split('/')[-1]
        cv2.imwrite(save_path, image_rgb_copy)
