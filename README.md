# face_anonymizer_AIthena
This is a tool that can be used to anonymise faces, specially that has been recorded by a vehicle to be used in CCAM applications. This tool, blurres faces of persons appearing in the recorded scenes.

In the first step, it performs a person detector, and in the second step it performs facial detection. For the second step, three different face recognizers are used. The results of the three face detectors are accumulated. To improve runtime or increase precision, only one or two face recognizers can be used. 

Furthermore, the thresholds of the detectors can be set to control recall and precision. 
Please feel free to adapt the thresholds in the classes PedestrianDetector, MTCNNDetector, FaceboxDetector and RetinaFaceDetector.


## Installation
Code was tested on Ubuntu 20.04
Python Version: 3.10

```
pip install -r requirements.txt
```

## Usage
Make sure that you specify the folder path to the images that are to be anonymized and the path of the output folder in which the images are to be saved.  

```
python main.py --input-path path/to/input/folder/ --output-path path/to/output/folder/
```

All three face detectors are used by default. To select only one or two face detectors use --face-detectors. To select e.g., MTCNN and FaceBoxes do: --face-detectors MTCNN,FaceBoxes 


## License
MIT / BSD-3-Clause license

Code is based on:
https://github.com/timesler/facenet-pytorch 
https://github.com/zisianw/FaceBoxes.PyTorch
https://github.com/pytorch/vision/blob/main/torchvision/models/detection/faster_rcnn.py
