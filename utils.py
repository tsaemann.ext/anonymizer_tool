import os
import cv2
import numpy as np
import torch


def ensure_directory_exists(path):
    if not os.path.exists(path):
        os.makedirs(path)
        print(f"Directory '{path}' created.")


def resize_images(images, resize_size):
    resized_images = []

    for img in images:
        current_height, current_width, _ = img.shape

        # Calculate the new dimensions to be 5 times divisible by 2
        new_width = int(np.ceil(current_width / 2 ** 5) * 2 ** 5)
        new_height = int(np.ceil(current_height / 2 ** 5) * 2 ** 5)

        # Check if either dimension is smaller than 128
        if new_width < resize_size or new_height < resize_size:
            # Calculate the scaling factor to ensure both dimensions are at least 128
            scaling_factor = max(resize_size / new_width, resize_size / new_height)
            # Upscale both dimensions with the same factor
            new_width = int(new_width * scaling_factor)
            new_height = int(new_height * scaling_factor)

        scaling_factor_width = current_width / new_width
        scaling_factor_height = current_height / new_height

        # Resize the image using OpenCV or any other library of your choice
        resized_img = cv2.resize(img, (new_width, new_height))
        resized_images.append([resized_img, current_height, current_width, scaling_factor_height, scaling_factor_width])

    return resized_images


def face_boxes_2_org_img(detected_ped_boxes, detected_faces_mtcnn=None, detected_faces_facebox=None,
                         detected_faces_retina=None):
    # add those detector that are specified from the user into the list
    detected_faces_all = [det for det in [detected_faces_mtcnn, detected_faces_facebox, detected_faces_retina]
                          if det is not None]
    new_coordinates = []
    for detected_faces in detected_faces_all:
        for index, (faces, ped) in enumerate(zip(detected_faces, detected_ped_boxes)):
            if faces[1] is not None and faces[1].nelement() != 0:
                face_x1, face_y1, face_x2, face_y2 = faces[0][0][0]
                ped_x1, ped_y1, ped_x2, ped_y2 = ped
                new_x1 = ped_x1 + face_x1 * faces[5]
                new_x2 = ped_x1 + face_x2 * faces[5]
                new_y1 = ped_y1 + face_y1 * faces[4]
                new_y2 = ped_y1 + face_y2 * faces[4]
                new_coordinates.append([int(new_x1), int(new_y1), int(new_x2), int(new_y2)])
            else:
                continue
    return new_coordinates


def visualize_boxes(image_rgb, image_path, new_coordinates, output_path):
    for new_coor in new_coordinates:
        cv2.rectangle(image_rgb, (new_coor[0], new_coor[1]), (new_coor[2], new_coor[3]), (0, 255, 0), 2)

    image_bgr = cv2.cvtColor(image_rgb, cv2.COLOR_RGB2BGR)
    ensure_directory_exists(output_path + '/All/')
    cv2.imwrite(output_path + '/All/' + image_path.split('/')[-1], image_bgr)


def blur_faces(image_rgb, image_path, new_coordinates, output_path, blur_px_extension=5):
    for new_coor in new_coordinates:
        x1, y1, x2, y2 = new_coor
        # enlarge the box by 5 px in all direction
        x1 = x1 - blur_px_extension
        x2 = x2 + blur_px_extension
        y1 = y1 - blur_px_extension
        y2 = y2 + blur_px_extension
        roi = image_rgb[y1:y2, x1:x2]

        # Apply a blur to the ROI
        blurred_roi = cv2.GaussianBlur(roi, (45, 45), 0)

        # Replace the original ROI with the blurred version
        image_rgb[y1:y2, x1:x2] = blurred_roi

    blurred_image = cv2.cvtColor(image_rgb, cv2.COLOR_RGB2BGR)
    cv2.imwrite(output_path + '/' + image_path.split('/')[-1], blurred_image)


def imread_cv2(image_path):
    org_image = cv2.imread(image_path)
    org_image_rgb = cv2.cvtColor(org_image, cv2.COLOR_BGR2RGB)
    return org_image_rgb
