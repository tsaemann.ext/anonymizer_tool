import cv2
import numpy as np
import torch
from torch.nn import functional as F
from torch.utils import model_zoo
from torchvision.ops import nms

from retinafaces.network import RetinaFace
from retinafaces.utils import prior_box, decode
from utils import resize_images, ensure_directory_exists


class RetinaFaceDetector:
    def __init__(self):
        self.confidence_threshold = 0.02
        self.nms_threshold = 0.1
        self.variance = [0.1, 0.2]
        self.device = torch.device("cuda")
        self.model = RetinaFace(name="Resnet50", pretrained=True, return_layers={"layer2": 1, "layer3": 2, "layer4": 3},
                                in_channels=256, out_channels=256).eval().to(device=self.device)
        url = ("https://github.com/ternaus/retinaface/releases/download/0.01/retinaface_resnet50_2020-07-20-f168fae3c"
               ".zip")
        state_dict = model_zoo.load_url(url, progress=True, map_location="cuda")
        self.model.load_state_dict(state_dict)

    def _pre_processing_retina(self, image):
        image = resize_images(image, 128)  # 128 worked pretty well for Kitti images
        image = [(element[0].transpose(2, 0, 1), *element[1:]) for element in image]
        image = [(np.float32(element[0]), *element[1:]) for element in image]
        image = [(torch.from_numpy(element[0]).to(self.device), *element[1:]) for element in image]
        image = [((element[0] / 255.0).unsqueeze(0), *element[1:]) for element in image]
        return image

    def get_bbox_detection(self, image):
        pre_image = self._pre_processing_retina(image)
        with torch.no_grad():
            results_list = []
            for img in pre_image:
                loc, conf, land = self.model(img[0])
                conf = F.softmax(conf, dim=-1)
                # create prior boxes according the input image size
                prior = prior_box(min_sizes=[[16, 32], [64, 128], [256, 512]], steps=[8, 16, 32], clip=False,
                                  image_size=(img[0].shape[2], img[0].shape[3])).to(device=self.device)

                boxes = decode(loc.data[0], prior, self.variance)
                scale_bboxes = torch.from_numpy(np.tile([img[0].shape[3], img[0].shape[2]], 2)).to(self.device)
                boxes *= scale_bboxes
                scores = conf[0][:, 1]

                # ignore low scores
                valid_index = torch.where(scores > self.confidence_threshold)[0]
                boxes = boxes[valid_index]
                scores = scores[valid_index]

                # do NMS
                keep = nms(boxes, scores, self.nms_threshold)
                boxes = boxes[keep, :].int()
                # handle case where no boxes are predicted
                if len(boxes) == 0:
                    boxes = None
                    scores = None

                results_list.append([[boxes], scores, *img[1:]])
            return results_list

    @staticmethod
    def visualize_face_detection(image_rgb, image_path, detected_faces, detected_ped_boxes, output_path):
        image_rgb_copy = np.copy(image_rgb)  # copy to avoid changing image outside this function (ndarray->mutable)
        # Plot bounding boxes
        for index, (faces, ped) in enumerate(zip(detected_faces, detected_ped_boxes)):
            if faces[1] is not None:
                face_x1, face_y1, face_x2, face_y2 = faces[0][0][0]
                ped_x1, ped_y1, ped_x2, ped_y2 = ped
                new_x1 = ped_x1 + face_x1 * faces[5]
                new_x2 = ped_x1 + face_x2 * faces[5]
                new_y1 = ped_y1 + face_y1 * faces[4]
                new_y2 = ped_y1 + face_y2 * faces[4]

                cv2.rectangle(image_rgb_copy, (int(new_x1.cpu().detach().numpy()), int(new_y1.cpu().detach().numpy())),
                              (int(new_x2.cpu().detach().numpy()), int(new_y2.cpu().detach().numpy())), (0, 0, 255), 2)
            else:
                continue

        image_rgb_copy = cv2.cvtColor(image_rgb_copy, cv2.COLOR_RGB2BGR)
        ensure_directory_exists(output_path + '/Retina/')
        save_path = output_path + '/Retina/' + image_path.split('/')[-1]
        cv2.imwrite(save_path, image_rgb_copy)
